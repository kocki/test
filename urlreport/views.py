# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import re
import requests
from bs4 import BeautifulSoup
from collections import Counter, OrderedDict
from django.views.generic import TemplateView
from django.views.generic.edit import FormView
from .forms import GetUrlForm


class GetUrlView(FormView):
    template_name = 'urlreport/urlgetform.html'
    form_class = GetUrlForm
    success_url = 'report/'

    def form_valid(self, form):
        self.success_url = '{}?url={}'.format(
            self.get_success_url(), form.cleaned_data['url'])
        return super(GetUrlView, self).form_valid(form)


class UrlReportView(TemplateView):
    template_name = 'urlreport/urlreport.html'
    url = None
    
    def get_page(self):
        try:
            page = requests.get(self.url)
            if page.status_code == 200:
                return page.content.decode('utf-8')
        except:
            import traceback
            traceback.print_exc()
        return None

    def get_context_data(self, **kwargs):
        kwargs = super(UrlReportView, self).get_context_data(**kwargs)
        kwargs['url'] = self.url

        # get page:
        page = self.get_page()
        if page:
            kwargs['page_size'] = len(page)

            html = BeautifulSoup(page, 'html5lib')
            kwargs['title'] = ' | '.join(html.title.find_all(string=True))
            kwargs['meta'] = [unicode(el) for el in html.find_all('meta')]

            # get all keywords from meta tag(s):
            meta_keywords = html.find_all('meta', attrs={'name': 'keywords'})
            if meta_keywords:
                keywords = []
                for tag in meta_keywords:
                    keywords += tag.attrs.get('content', '').split(',')
                meta_keywords = map(lambda k: k.lower().strip(), keywords)

            # words counting:
            html_text = html.body.get_text().lower()
            rgx = re.compile("([\w][\w']*\w)")
            html_text_by_words = rgx.findall(html_text)

            # In real live case we probably exclude all one leeter words and
            # possible some other with exclude list. Here we count all:
            kwargs['words_count'] = len(html_text_by_words)
            counted_words = Counter(html_text_by_words)
            kwargs['unique_words_count'] = len(counted_words.keys())

            words = {}
            for word, count in counted_words.items():
                if count in words:
                    words[count].append(word)
                else:
                    words[count] = [word]

            # We want 5 most common words, but what if have more with same most
            # count? Here I get 5 as most common, than longest, than sorted
            # alphabetically:
            # kwargs['most_common_words'] = OrderedDict(
            word_count = 5
            most_common_words = []
            for key in sorted(words.keys(), reverse=True):
                word_count -= len(words[key])
                if word_count >= 0:
                    most_common_words += map(
                        lambda k: '{}({})'.format(k, key), words[key])
                else:
                    # sort by length desc, then alphabetically
                    words[key].sort()
                    words[key].sort(key=len, reverse=True)
                    most_common_words += map(
                        lambda k: '{}({})'.format(k, key),
                        words[key][:word_count])
                if word_count <= 0:
                    break

            kwargs['most_common_words'] = most_common_words
            kwargs['not_used_meta'] = set(meta_keywords) - set(
                html_text_by_words)

            kwargs['atags'] = []
            for tag in html.find_all('a'):
                kwargs['atags'].append({
                    'href': tag.attrs.get('href'),
                    'content': tag.text.strip().replace('\n', ' '),
                })
        else:
            kwargs['error'] = 'Can not read page!'
        return kwargs

    def get(self, request, *args, **kwargs):
        self.url = request.GET.get('url', None)
        return super(UrlReportView, self).get(request, *args, **kwargs)
