from django import forms


class GetUrlForm(forms.Form):
    url = forms.URLField()
